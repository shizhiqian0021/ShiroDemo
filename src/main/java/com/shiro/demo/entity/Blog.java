package com.shiro.demo.entity;

import java.io.Serializable;
import java.util.Date;

public class Blog implements Serializable {

    private final static Long serialVersionUID  = 5844966552335546L;

    private String id;

    private String userName;

    private String logInfo;

    private Date releaseTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getUserName() {
        return userName;
    }


    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLogInfo() {
        return logInfo;
    }

    public void setLogInfo(String logInfo) {
        this.logInfo = logInfo;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }
}

package com.shiro.demo.config;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.springframework.data.redis.core.RedisTemplate;

public class RedisCacheManager implements CacheManager {

    public RedisCacheManager(Cache cache) {
        this.cache = cache;
    }

    private Cache cache;

    @Override
    public <K, V> Cache<K, V> getCache(String s) throws CacheException {
        return cache;
    }
}

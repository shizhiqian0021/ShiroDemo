package com.shiro.demo.config;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

public class RedisCache implements Cache {

    public RedisCache(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    RedisTemplate redisTemplate;

    private final static String CACHE_PREFIX = "cache-prefix:";

    @Override
    public Object get(Object o) throws CacheException {
        if (o instanceof Serializable && o instanceof String){
            Object o1 = redisTemplate.opsForValue().get(CACHE_PREFIX + (String) o);
            return o1;
        }
        return null;
    }

    @Override
    public Object put(Object o, Object o2) throws CacheException {
        if (o instanceof Serializable && o2 instanceof Serializable && o instanceof String){
            redisTemplate.opsForValue().set(CACHE_PREFIX + (String) o, o2);
            return true;
        }
        return false;
    }

    @Override
    public Object remove(Object o) throws CacheException {
        if (o instanceof Serializable && o instanceof String) {
            Boolean delete = redisTemplate.delete(CACHE_PREFIX + (String) o);
            return delete;
        }
        return false;
    }

    @Override
    public void clear() throws CacheException {

    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public Set keys() {
        return null;
    }

    @Override
    public Collection values() {
        return null;
    }
}

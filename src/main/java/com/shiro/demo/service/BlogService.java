package com.shiro.demo.service;

import com.shiro.demo.entity.Blog;

import java.util.List;

public interface BlogService {
    List<Blog> getAllBlog();

    Boolean addBlog(Blog blog);

//    Boolean deleteBlog(Blog blog);

    Boolean update(Blog blog);
}

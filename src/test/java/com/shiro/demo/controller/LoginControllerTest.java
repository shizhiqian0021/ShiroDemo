package com.shiro.demo.controller;

import com.shiro.demo.entity.User;
import com.shiro.demo.util.ShiroUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class LoginControllerTest {
    private User user;

    @Autowired
    RedisTemplate redisTemplate;

    @Before
    public void before() {
        user = new User();
        user.setUserName("sunwukong");
        user.setPassWord("123456");
        user.setId(ShiroUtil.UUID());
        Set<String> hashSet = new HashSet<>();
        hashSet.add("admin");
        hashSet.add("user");
        user.setRoles(hashSet);
        redisTemplate.opsForValue().set(user.getUserName(), user);
    }

    @Test
    public void index() {
        System.out.println("123");
    }
}